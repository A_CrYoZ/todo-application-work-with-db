const { Router } = require("express");
const axios = require("axios");

const { API_ROUTE, AUTH_TOKEN } = require("../../configs/api_config");

const config = {
  headers: {
    Authorization: `Bearer ${AUTH_TOKEN}`,
  },
};

const router = new Router();

module.exports = () => {
  router.get("/", async (request, response, next) => {
    try {
      // Get data from REST API
      const result = await axios.get(`${API_ROUTE}/tasks/done`, config).catch((error) => {
        if (error.response.status !== 404) {
          next(error);
        }
      });

      const data = result?.data;

      const tasksListFilled = data?.length > 0;

      response.render("layout", { template: "done", tasksListFilled, tasksList: data });
    } catch (error) {
      next(error);
    }
  });

  return router;
};
