const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");
const path = require("path");
const cookieSession = require("cookie-session");

const routes = require("./routes/index");

const app = express();

// set up a cookie session
app.use(
  cookieSession({
    name: "session",
    keys: ["kofalsdmlSANkmals231lmsdl", "KAsdmKAasldAO21M34"],
  })
);

// Set body-parsers
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// specify a template engine to use
app.set("view engine", "ejs");
// tell express that our front files will be placed in views dir
app.set("views", path.join(__dirname, "../views"));
// Add middleware to use static resources
app.use(express.static(path.join(__dirname, "../assets")));

// Set routes middleware
app.use(routes());

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () =>
    new Promise((resolve) => {
      server.stop(resolve);
    });

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log("Express server is listening on http://localhost:3000");
      resolve(stopServer);
    });
  });
};
